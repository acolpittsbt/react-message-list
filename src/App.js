import React, {Component} from 'react';
import MessageList from './components/message-list/MessageList';


class App extends Component {
  constructor() {
    super();

    let mock_data = [
      {
        'Twitter': {
          'user': {
            'profile_image_url': 'http://gravatar.com/avatar/44842684fe6e0b841b160722e36d534a',
            'screen_name': '@acolpitts',
            'name': 'Adam Colpitts'

          },
          'content': 'React/ES6 implementation of a basic message list component. @hootsuite #letsmakeadeal'
        }
      },
      {
        'Facebook': {
          'from': {
            'name': 'Jonathon Smith',
            'handle': 'jon.smith',
            'image_url': 'https://www.gravatar.com/avatar/205e460b479e2e5b48aec07710c08d50?f=y'
          },
          'message': 'Do the people inside mascot costumes also smile when they have their photos taken?'
        }
      }
    ];

    this.state = {
      data: mock_data
    };

    // mock data stream
    let self = this;
    let i = 0;
    let timer = setInterval(function () {
      mock_data.push(mock_data[Math.round(Math.random())]);
      self.setState({data: mock_data});
      if (++i === 5) {
        window.clearInterval(timer);
      }
    }, 2000)

  }

  render() {
    return (
      <div className="container">
          <MessageList messages={this.state.data}/>
      </div>
    );
  }
}

export default App;
