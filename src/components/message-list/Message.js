import React from 'react';

import './Message.css';

class Message extends React.Component {
  render() {
    return (
      <li className="message">
        <img src={this.props.avatar}
             className="avatar"
             alt="avatar"/>
        <blockquote>
          <cite>
            <a href="#" className="author">{this.props.author}</a>
            <span className="screen-name">{this.props.screen_name}</span>
          </cite>
          <span className="content">{this.props.content}</span>
        </blockquote>
      </li>
    )
  }
}

export default Message