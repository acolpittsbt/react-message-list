import React from 'react';
import Message from './Message';

import './MessageList.css';

class MessageList extends React.Component {
  constructor() {
    super();
    this.state = {
      messages: []
    }
  }

  componentWillMount() {
    this.parseMessages(this.props.messages);
  }

  componentWillReceiveProps(nextProps) {
    this.parseMessages(nextProps.messages);
  }

  parseMessages(messages) {
    let cleanMessages = messages.map(function (msg, i) {
      if (msg['Twitter']) {
        return this.parseTwitter(msg['Twitter']);
      } else if (msg['Facebook']) {
        return this.parseFacebook(msg['Facebook']);
      } else {
        throw new Error("Unknown message format received!");
      }
    }, this);

    this.setState({messages: cleanMessages})
  }

  parseTwitter(tweet) {
    return {
      'author': tweet.user.name,
      'screen_name': tweet.user.screen_name,
      'avatar': tweet.user.profile_image_url,
      'content': tweet.content
    };
  }

  parseFacebook(post) {
    return {
      'author': post.from.name,
      'screen_name': post.from.handle,
      'avatar': post.from.image_url,
      'content': post.message
    };
  }

  render() {
    let messages = this.state.messages.map(function (message, i) {
      return (
        <Message key={i}
                 avatar={message.avatar}
                 author={message.author}
                 screen_name={message.screen_name}
                 content={message.content}/>
      )
    });

    return (
      <ul className="message-list">
        {messages}
      </ul>
    )
  }
}

MessageList.PropTypes = {
  messages: React.PropTypes.array.isRequired
};

export default MessageList